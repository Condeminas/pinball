﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class StartController : MonoBehaviour {
    
	void Update () {
        if (Input.GetKey(KeyCode.Space))
        {
            SceneManager.LoadScene("Pinball");
        }


        if (Input.GetKey(KeyCode.Escape))
        {
            Application.Quit();
        }

    }
}
