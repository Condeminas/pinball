﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BumperControl : MonoBehaviour {
	public float force = 100.0f;
	public float forceRadius = 1.0f;
      
    void OnCollisionEnter(){
		foreach (Collider col in Physics.OverlapSphere(transform.position, forceRadius)) {
			if (col.GetComponent<Rigidbody>()) 
			{
                col.attachedRigidbody.AddExplosionForce (force, transform.position, forceRadius);
			}
		}
	}
}
