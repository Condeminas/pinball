﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class BallController : MonoBehaviour {
    private Vector3 initialPos;
    private Quaternion initialRot;
    public int lives;
    static int score;
    static int maxScore = 999;
    public Text LivesCount;
    public Text ScoreCount;
    int targetHit;
    public GameObject target;
    GameObject[] trg;
    Animator anim;
    Scene currentScene;

    new AudioSource audio;
    public AudioClip BumperSound;
    public AudioClip TargetSound;
    public AudioClip HomeRunSound;
    public AudioClip StrikeSound;
    public AudioClip StrikeOutSound;
    
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.name=="Barrera")
        {
            Debug.Log("wololo");           
            other.gameObject.SetActive(true);
        }
    }
    private void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.CompareTag("Abyss"))
        {
            /*
            if (lives == 1)
            {
                audio.clip = StrikeOutSound;
                audio.Play();
            }
            else
            {*/
                 audio.clip = StrikeSound;
                audio.Play();

                lives--;
                Debug.Log("Goodbyeeee");
                Debug.Log(lives);
                this.transform.position = initialPos;
                this.transform.rotation = initialRot;
            //}
           
        }

        else if (other.gameObject.CompareTag("Bumper"))
        {
            audio.clip = BumperSound;
            audio.Play();

            score = score + 10;
            Debug.Log(score);

        }

        else if (other.gameObject.CompareTag("Target"))
        {
            audio.clip = TargetSound;
            audio.Play();    

            anim = other.gameObject.GetComponent<Animator>();
            anim.SetBool("Apagado", true);           
            targetHit++;
            score = score + 50;                          
        }
    }
     void Start()
    {
        lives = 3;
        currentScene = SceneManager.GetActiveScene();
        targetHit = 0;
        initialPos = transform.position;
        initialRot = transform.rotation;

        audio = GetComponent<AudioSource>();
    }

    void Awake()
    {
       score = 0;
       trg = GameObject.FindGameObjectsWithTag("Target");
    }
    // Update is called once per frame
    void Update () {

        if (currentScene.name == "Pinball")
        {
            ScoreCount.text = "  Score: " + score.ToString();
            LivesCount.text = "  Lives: " + lives.ToString();

             if (targetHit == 3)
                    {
                        audio.clip = HomeRunSound;
                        audio.Play();
                        score = score * 3;
                        targetHit = 0;
                        foreach (GameObject targ in trg)
                        {
                            anim = targ.gameObject.GetComponent<Animator>();
                            anim.SetBool("Apagado", false);
                        }
                    }

                    if (score > maxScore)
                        {
                            maxScore = score;
                        }

                    if (Input.GetKey(KeyCode.LeftShift))
                    {
                        ScoreCount.text = "  MAX Score: " + maxScore.ToString();
                    }


                    if (lives <= 0)
                    {
                        audio.clip = StrikeOutSound;
                        audio.Play();
                        Debug.Log("MAX SCORE " + maxScore);
                        ScoreCount.text = "  MAX Score: " + maxScore.ToString();
                        SceneManager.LoadScene("Credits");
                        //Application.LoadLevel("Credits");
                    }

        }

        else if(currentScene.name == "Credits")
        {
            ScoreCount.text = "  MAX Score: " + maxScore.ToString();
            LivesCount.text = " GAME OVER";
        }        
        
       
	}
}
